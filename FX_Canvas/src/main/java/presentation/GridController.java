package presentation;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Slider;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.TreeSet;

/**
 * Created: 26.04.2023
 *
 * @author karre
 */
public class GridController implements Initializable {
    @FXML
    private Canvas canva;

    private GraphicsContext gc;

    @FXML
    private Slider zoomSlider;

    @FXML
    private AnchorPane anchorPane;

    @FXML
    private CheckBox gridCheckbox;

    @FXML
    private Button btnClear;

    @FXML
    private Button btnNext;

    @FXML
    private Button btnStart;

    @FXML
    private Button btnStop;

    @FXML
    private Pane paneCanva;

    @FXML
    private Pane paneFooter;

    @FXML
    private GridPane gridFooter;

    @FXML
    private GridPane gridFooter2;


    private final Set<RectangleSave> allRecs = new TreeSet<>();

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        gc = canva.getGraphicsContext2D();
        getGrid(getSliderSize());

        zoomSlider.valueProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observableValue, Number number, Number t1) {
                clearGrid(false);
                if (gridCheckbox.selectedProperty().get()) {
                    getGrid(t1.intValue());
                }
                drawSavedRectangles();
            }
        });
        gridCheckbox.selectedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observableValue, Boolean aBoolean, Boolean t1) {
                if (aBoolean) {
                    clearGrid(false);
                    drawSavedRectangles();
                } else {
                    getGrid(getSliderSize());
                    drawSavedRectangles();
                }
            }
        });
        canva.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                createRectangle(mouseEvent,gridCheckbox.isSelected());
            }
        });
        canva.addEventHandler(MouseEvent.MOUSE_DRAGGED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                createRectangle(mouseEvent,gridCheckbox.isSelected());
            }
        });
        btnClear.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                clearGrid(true);
                getGrid(getSliderSize());
            }
        });
        anchorPane.widthProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observableValue, Number number, Number t1) {
                canva.setWidth(anchorPane.getWidth()-200);
                getGrid(getSliderSize());
                drawSavedRectangles();
            }
        });
        anchorPane.heightProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observableValue, Number number, Number t1) {
                canva.heightProperty().bind(paneCanva.heightProperty());
                gridFooter.layoutYProperty().set(0+(t1.doubleValue()-541.0));
                gridFooter2.layoutYProperty().set(0+(t1.doubleValue()-541.0));
                getGrid(getSliderSize());
            }
        });

    }

    public void nextStep(){
        for (int i = 0; i < canva.getHeight(); i=i+getSliderSize()) {
            for (int j = 0; j < canva.getWidth(); j=j+getSliderSize()) {
                if (rectangleExists(i/getSliderSize(),j/getSliderSize())){

                }
            }
        }
    }

    public void createRectangle(MouseEvent mouseEvent,boolean grid){
        if (rectangleExists(mouseEvent)) {
            removeRectangle(mouseEvent);
            clearGrid(false);
            if (grid){
                getGrid(getSliderSize());
            }
            drawSavedRectangles();

        } else {
            paint(mouseEvent.getX(), mouseEvent.getY(), getSliderSize());
        }
    }

    public boolean rectangleExists(MouseEvent mouseEvent) {
        return allRecs.stream().filter(t -> t.x == getColIndex(mouseEvent.getX()) && t.y == getRowIndex(mouseEvent.getY())).toList().size() > 0;
    }

    public boolean rectangleExists(int col, int row){
        return allRecs.stream().filter(t -> t.x == col && t.y == row).toList().size() > 0;
    }

    public void removeRectangle(MouseEvent mouseEvent) {
        allRecs.removeIf(t -> t.x == getColIndex(mouseEvent.getX()) && t.y == getRowIndex(mouseEvent.getY()));
    }

    public int getSliderSize() {
        return (int) zoomSlider.getValue();
    }

    public void getGrid(int space) {
        for (int x = 0; x < canva.getWidth(); x += space) {
            gc.strokeLine(x, 0, x, canva.getHeight());
        }
        for (int y = 0; y < canva.getHeight(); y += space) {
            gc.strokeLine(0, y, canva.getWidth(), y);
        }
    }

    public void clearGrid(boolean clearList) {
        if (clearList){
            gc.clearRect(0, 0, canva.getWidth(), canva.getHeight());
            allRecs.clear();
        }else {
            gc.clearRect(0, 0, canva.getWidth(), canva.getHeight());
        }

    }

    public int getColIndex(double x) {
        return (int) (x / getSliderSize());
    }

    public int getRowIndex(double y) {
        return (int) (y / getSliderSize());
    }

    public void paint(double x, double y, int size) {
        gc.fillRect(getColIndex(x) * size, getColIndex(y) * size, size, size);
        allRecs.add(new RectangleSave(getColIndex(x), getRowIndex(y)));
    }

    public void drawSavedRectangles() {
        allRecs.forEach((n) -> {
            if (n.x * getSliderSize() < canva.getWidth() || n.y * getSliderSize() < canva.getHeight()) {
                paint(n.x * getSliderSize(), n.y * getSliderSize(), (int) zoomSlider.getValue());
            }

        });
    }
}
