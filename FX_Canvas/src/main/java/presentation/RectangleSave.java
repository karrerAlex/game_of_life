package presentation;

/**
 * Created: 10.05.2023
 *
 * @author karre
 */
public class RectangleSave implements Comparable<RectangleSave>{
    int x;
    int y;

    public RectangleSave(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    @Override
    public String toString() {
        return "RectangleSave{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }


    @Override
    public int compareTo(RectangleSave o) {
        if (x - o.x == 0) {
            return y - o.y;
        } else {
            return x - o.x;
        }
    }
}
